# coding:utf-8
import hmac
import hashlib

# 摘要算法又称哈希算法、散列算法。通过一个函数，把任意长度的数据转换为一个长度固定的数据串（通常用16进制的字符串表示）
# 摘要算法不是加密算法，不能用于加密（因为无法通过摘要反推明文），只能用于防篡改，
# 单向计算特性决定了可以在不存储明文口令的情况下验证用户口令
# SHA1的结果是160 bit字节，通常用一个40位的16进制字符串表示。
# 比SHA1更安全的算法是SHA256和SHA512，不过越安全的算法越慢，而且摘要长度更长

m = hashlib.md5()
m.update(b"Nobody inspects")
m.update(b"the-Salt")  # 加盐，防止被破解
m.update(b" the spammish repetition")
print(m.hexdigest())

print(type(m.hexdigest()))

h = hmac.new('天王盖地虎'.encode('utf8'))  # hmac 必须要加盐
h.update('hello'.encode('utf8'))
h.update('world'.encode('utf8'))
print(h.hexdigest())

h1 = hmac.new('天王盖地虎'.encode('utf8'))  # hmac 必须要加盐
h1.update('helloworld'.encode('utf8'))
# h1.update('world'.encode('utf8'))
print(h1.hexdigest())


if __name__ == '__main__':
	filename = "E:\\2018智能项目\\ServiceIntelligentResultProcess\\ConfigInfo.xml"
	m = hashlib.md5()
	with open(filename, "rb") as fp:
		while True:
			blk = fp.read(4096)
			if not blk:
				break
			m.update(blk)
	print(m.hexdigest(), filename)  # 生成文件的md5 可以用来校验文件是否被篡改
