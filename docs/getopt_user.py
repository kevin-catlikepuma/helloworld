# coding:utf-8
import sys
import getopt

"""
可以查看源码，该模块的源码很简单。
python getopt_user.py -i 127.0.0.1 -p 80 55 66
“hp:i:”
短格式 --- h 后面没有冒号：表示后面不带参数，p：和 i：后面有冒号表示后面需要参数
["help","ip=","port="]
长格式 --- help后面没有等号=，表示后面不带参数，其他三个有=，表示后面需要参数
返回值 options 是个包含元祖的列表，每个元组是分析出来的格式信息，比如 [('-i','127.0.0.1'),('-p','80')] ;
 args 是个列表，包含那些没有‘-’或‘--’的参数，比如：['55','66']
"""

if __name__ == "__main__":
	try:
		para = sys.argv[1:]
		options, args = getopt.getopt(para, "hp:i:", ["help", "ip=", "port="])
	except getopt.GetoptError:
		sys.exit()
    
    print(para)
	print type(options), "options:", options
	print type(args), "args:", args
	for name, value in options:
		if name in ("-h", "--help"):
			print "call help: about usage of this method"
		elif name in ("-i", "--ip"):
			print "ip is --", value
		elif name in ("-p", "--port"):
			print "port is --", value
		else:
			print "error parameter..."
